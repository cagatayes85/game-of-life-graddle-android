package cluster.istruct.gol .app.activity.main;


import android.graphics.Point;
import android.view.Display;

import com.squareup.otto.Subscribe;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import cluster.istruct.gol.game.activity.EventRepository;
import cluster.istruct.gol.game.automata.Filler;
import cluster.istruct.gol.game.automata.GameOfLifePrefab;
import cluster.istruct.gol.game.controller.GameController;
import cluster.istruct.gol.game.controller.ParameterBuilder;
import cluster.istruct.gol.game.controller.ParameterInitializer;
import cluster.istruct.gol.game.logic.CloseNeighbourRelation;
import cluster.istruct.gol.game.painter.UnitPainter;
import cluster.istruct.gol.game.unit.IUnit;
import cluster.istruct.gol.util.EventBus;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

@EBean
public class MainActHelper {
    private GameController gameController;

    @RootContext
    MainActivity activityInstance;

    void onActivityResume() {
        EventBus.getInstance().register(this);
    }

    void onActivityPause() {
        EventBus.getInstance().unregister(this);
    }

    protected void startGame() {
        ParameterInitializer parameterInitializer = new ParameterBuilder().create(returnGridSize(false, 200, 200), 20, new Filler(0.10f, IUnit.LIVING), new UnitPainter(), 15);
        gameController = new GameController(
                activityInstance.teselView,
                new GameOfLifePrefab(),
                parameterInitializer
        );
        gameController.Start();
    }

    void onRestartGame() {
        EventBus.getInstance().post(EventRepository.getBasics().new Restart());
    }


    void onPause() {
        EventBus.getInstance().post(EventRepository.getBasics().new Pause());
        setUiPausedState(true);
    }

    void onResume() {
        EventBus.getInstance().post(EventRepository.getBasics().new Resume());
        setUiPausedState(false);
    }

    private void setUiPausedState(boolean isPaused) {
        activityInstance.resume.setVisibility(isPaused ? VISIBLE : GONE);
        activityInstance.pause.setVisibility(isPaused ? GONE : VISIBLE);
        activityInstance.paused = isPaused;
    }

    void onResetGame() {
        EventBus.getInstance().post(EventRepository.getBasics().new Reset());
    }

    @Subscribe
    public void onRulesChanged(CloseNeighbourRelation rule) {
        gameController.getAutomat().setILogic(rule);
    }

    private Point returnGridSize(boolean custom, int x, int y) {
        if (!custom) {
            Display display = activityInstance.getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            return size;
        } else
            return new Point(x, y);
    }

}
