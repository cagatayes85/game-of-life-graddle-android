package cluster.istruct.gol.game.converter;

import cluster.istruct.gol.game.logic.ILogic;
import cluster.istruct.gol.game.unit.IUnit;
import cluster.istruct.gol.game.grid.IGridView;


public interface IGridConverter<T extends IUnit> {
    public void transform(IGridView<T> IGridView, ILogic<T> ILogic);
}
