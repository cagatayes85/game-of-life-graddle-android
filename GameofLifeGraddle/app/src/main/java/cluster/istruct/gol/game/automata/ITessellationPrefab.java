package cluster.istruct.gol.game.automata;

public interface ITessellationPrefab {
    ITessellation<?> create(int gridX, int gridY);
}
