package cluster.istruct.gol.game.painter;

import android.graphics.Paint;

public interface IPainter {
    Paint getPaint(int state);
}
