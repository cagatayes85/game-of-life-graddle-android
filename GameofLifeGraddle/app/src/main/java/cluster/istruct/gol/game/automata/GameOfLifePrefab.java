package cluster.istruct.gol.game.automata;

public class GameOfLifePrefab implements ITessellationPrefab {
    @Override
    public ITessellation<?> create(int gridX, int gridY) {
        return new GameOfLife(gridX, gridY);
    }
}
