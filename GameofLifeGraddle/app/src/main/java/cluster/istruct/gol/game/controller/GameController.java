package cluster.istruct.gol.game.controller;

import cluster.istruct.gol.game.automata.ITessellation;
import cluster.istruct.gol.game.automata.ITessellationPrefab;
import cluster.istruct.gol.game.view.TessellationView;
public class GameController {
    private final ITessellation automaton;
    private final TessellationView tessellationView;
    private final ParameterInitializer params;

    public GameController(TessellationView tessellationView, ITessellationPrefab factory, ParameterInitializer params) {
        this.tessellationView = tessellationView;
        this.params = params;

        automaton = createAutomat(factory, params);
    }

    public ITessellation getAutomat() {
        return automaton;
    }

    public void Start() {
        initView(
                tessellationView,
                automaton,
                params
        );
    }

    public void pauseGame() {
        //  TO DO
    }

    public void resumeGame() {
        // TO DO
    }

    private ITessellation createAutomat(ITessellationPrefab factory, ParameterInitializer params) {
        ITessellation<?> automaton = factory.create(
                params.getGridSizeX(),
                params.getGridSizeY()
        );

        automaton.randomFill(params.getFiller());

        return automaton;
    }

    private static void initView(TessellationView tessellationView, ITessellation automaton, ParameterInitializer params) {
        tessellationView.init(
                automaton,
                params
        );
    }
}
