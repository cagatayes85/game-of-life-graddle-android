package cluster.istruct.gol.game.automata;

import cluster.istruct.gol.game.logic.ILogic;
import cluster.istruct.gol.game.unit.SimpleIUnit;
import cluster.istruct.gol.game.unit.IUnitPrefab;
import cluster.istruct.gol.game.unit.SimpleIUnitPrefab;
import cluster.istruct.gol.game.logic.ConwayILogic;

public class GameOfLife extends lDiscreteITessellation<SimpleIUnit> {
    public GameOfLife(int gridSizeX, int gridSizeY) {
        super(gridSizeX, gridSizeY);
    }

    @Override
    protected IUnitPrefab<SimpleIUnit> getFactory() {
        return new SimpleIUnitPrefab();
    }

    @Override
    public ILogic<SimpleIUnit> createRule() {
        return new ConwayILogic();
    }
}
