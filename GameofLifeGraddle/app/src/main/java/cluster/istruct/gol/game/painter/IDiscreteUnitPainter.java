package cluster.istruct.gol.game.painter;

import android.graphics.Color;
import android.graphics.Paint;

import java.util.HashMap;
import java.util.Map;

public class IDiscreteUnitPainter implements IPainter {
    protected final Map<Integer, Paint> paintMap;

    public IDiscreteUnitPainter() {
        paintMap = new HashMap<>(2);
    }

    protected Paint createPaint(String color) {
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.parseColor(color));

        return paint;
    }

    @Override
    public Paint getPaint(int state) {
        return paintMap.get(state);
    }
}
