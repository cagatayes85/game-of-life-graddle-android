package cluster.istruct.gol.game.grid;

import cluster.istruct.gol.game.unit.IUnit;

public interface IGridView<T extends IUnit> {
    int getSizeX();
    int getSizeY();
    T getCell(int x, int y);
    void putCell(T cell);
}
