package cluster.istruct.gol.game.painter;

import cluster.istruct.gol.game.unit.IUnit;

public class UnitPainter extends IDiscreteUnitPainter {
    public UnitPainter() {
        super();
        paintMap.put(IUnit.DEAD, createPaint("#000000"));
        paintMap.put(IUnit.LIVING, createPaint("#a4c639"));
    }

}
