package cluster.istruct.gol.game.controller;

import android.graphics.Point;

import cluster.istruct.gol.game.automata.Filler;
import cluster.istruct.gol.game.painter.IPainter;

public class ParameterInitializer {
    private final Point displaySize;
    private final int cellSizeInPixels;
    private final Filler filler;
    private final IPainter iPainter;
    private final int fps;

    public ParameterInitializer(Point displaySize, int cellSizeInPixels, Filler filler, IPainter iPainter, int fps) {
        this.displaySize = displaySize;
        this.cellSizeInPixels = cellSizeInPixels;
        this.filler = filler;
        this.iPainter = iPainter;
        this.fps = fps;
    }

    public int getCellSize() {
        return cellSizeInPixels;
    }

    public int getGridSizeX() {
        return displaySize.x / cellSizeInPixels;
    }

    public int getGridSizeY() {
        return displaySize.y / cellSizeInPixels;
    }

    public Filler getFiller() {
        return filler;
    }

    public IPainter getiPainter() {
        return iPainter;
    }

    public int getFps() {
        return fps;
    }
}
