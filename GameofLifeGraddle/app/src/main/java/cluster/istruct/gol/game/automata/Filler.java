package cluster.istruct.gol.game.automata;

public class Filler {
    private final int state;
    private final float percent;

    public Filler(float percent, int state) {
        this.percent = percent;
        this.state = state;
    }

    public float getRstio() {
        return percent;
    }

    public int getState() {
        return state;
    }
}
