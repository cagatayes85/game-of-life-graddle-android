package cluster.istruct.gol.game.unit;

public class SimpleIUnitPrefab implements IUnitPrefab<SimpleIUnit> {
    @Override
    public SimpleIUnit create(int x, int y) {
        return new SimpleIUnit(x, y, SimpleIUnit.DEAD);
    }
}
