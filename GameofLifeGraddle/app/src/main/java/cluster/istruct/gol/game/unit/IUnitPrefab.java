package cluster.istruct.gol.game.unit;

public interface IUnitPrefab<T extends IUnit> {
    T create(int x, int y);
}
