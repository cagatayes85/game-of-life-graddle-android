package cluster.istruct.gol.game.grid;

import cluster.istruct.gol.game.unit.IUnit;
import cluster.istruct.gol.game.unit.IUnitPrefab;

public class BoundlessIGridViewHandler<T extends IUnit> implements IGridViewHandler<T> {
    private final int x;
    private final int y;
    private final IUnitPrefab<T> IUnitPrefab;
    IGridView<T> currentIGridView;

    public BoundlessIGridViewHandler(int x, int y, IUnitPrefab<T> IUnitPrefab) {
        this.x = x;
        this.y = y;
        this.IUnitPrefab = IUnitPrefab;
        currentIGridView = createNew();
    }

    @Override
    public IGridView<T> getCurrent() {
        return currentIGridView;
    }

    @Override
    public IGridView<T> createNew() {
        return new BoundlessGridView<T>(x, y, IUnitPrefab);
    }

    @Override
    public void setCurrent(IGridView<T> IGridView) {
        if (IGridView instanceof BoundlessGridView) {
            currentIGridView = IGridView;

        } else {
            currentIGridView = new BoundlessGridView<T>(IGridView, IUnitPrefab);
        }
    }
}
