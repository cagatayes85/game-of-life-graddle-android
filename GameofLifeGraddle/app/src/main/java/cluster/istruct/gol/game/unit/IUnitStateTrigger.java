package cluster.istruct.gol.game.unit;

import cluster.istruct.gol.game.activity.EventRepository;

public interface IUnitStateTrigger {

    void onUnitStateChanged(EventRepository.CellStateChange cellStateChange);

}
