package cluster.istruct.gol.game.converter;

import android.graphics.Point;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import cluster.istruct.gol.game.logic.ILogic;
import cluster.istruct.gol.game.unit.IUnit;
import cluster.istruct.gol.game.grid.IGridView;
import hugo.weaving.DebugLog;

public class ThreadedIGridConverter<T extends IUnit> implements IGridConverter<T> {
    private final int threadCount;
    private ExecutorService executorService;
    private CountDownLatch countDownLatch;
    private int[][] stateChanges;

    public ThreadedIGridConverter() {
        this(4);
    }

    public ThreadedIGridConverter(int threadCount) {
        this.threadCount = threadCount;
        executorService = Executors.newFixedThreadPool(threadCount);
    }

    @Override
    public final void transform(IGridView<T> IGridView, ILogic<T> ILogic) {
        resetStateChanges(IGridView);
        doTransform(IGridView, ILogic);
    }

    protected void resetStateChanges(IGridView<T> IGridView) {
        if (stateChanges == null) {
            stateChanges = new int[IGridView.getSizeY()][IGridView.getSizeX()];
        } else {
            doResetStateChanges(IGridView);
        }
    }

    private void doResetStateChanges(IGridView<T> IGridView) {
        for (int j = 0; j < IGridView.getSizeY(); j++) {
            for (int i = 0; i < IGridView.getSizeX(); i++) {
                stateChanges[j][i] = 0;
            }
        }
    }

    @DebugLog
    protected void doTransform(IGridView<T> IGridView, ILogic<T> ILogic) {
        resetLatch();
        threadedCompute(IGridView, ILogic);
        awaitLatch();

        resetLatch();
        threadedApply(IGridView, ILogic);
        awaitLatch();
    }

    private void resetLatch() {
        countDownLatch = new CountDownLatch(threadCount);
    }

    @DebugLog
    private void threadedCompute(final IGridView<T> IGridView, final ILogic<T> ILogic) {
        final int slice = IGridView.getSizeX() / threadCount;

        for (int i = 0; i < threadCount; i++) {
            final int f = i;
            executorService.submit(new Runnable() {
                @Override
                public void run() {
                    computeNewGrid(IGridView, ILogic, getSubGridStartPoint(f, slice), getSubGridEndPoint(f, slice, IGridView));
                    countDownLatch.countDown();
                }
            });
        }
    }

    protected Point getSubGridStartPoint(int f, int slice) {
        int minX = f * slice;

        return new Point(minX, 0);
    }

    protected Point getSubGridEndPoint(int f, int slice, IGridView<T> IGridView) {
        int maxX = f < threadCount - 1 ? (f + 1) * slice : IGridView.getSizeX();

        return new Point(maxX, IGridView.getSizeY());
    }

    @DebugLog
    protected void computeNewGrid(IGridView<T> IGridView, ILogic<T> ILogic, Point min, Point max) {
        for (int j = min.y; j < max.y; j++) {
            for (int i = min.x; i < max.x; i++) {
                stateChanges[j][i] = ILogic.apply(IGridView, i, j);
            }
        }
    }

    @DebugLog
    private void awaitLatch() {
        try {
            countDownLatch.await();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @DebugLog
    private void threadedApply(final IGridView<T> IGridView, final ILogic<T> ILogic) {
        final int slice = IGridView.getSizeX() / threadCount;

        for (int i = 0; i < threadCount; i++) {
            final int f = i;
            executorService.submit(new Runnable() {
                @Override
                public void run() {
                    applyNewGrid(IGridView, getSubGridStartPoint(f, slice), getSubGridEndPoint(f, slice, IGridView));
                    countDownLatch.countDown();
                }
            });
        }
    }

    @DebugLog
    protected void applyNewGrid(IGridView<T> IGridView, Point min, Point max) {
        for (int j = min.y; j < max.y; j++) {
            for (int i = min.x; i < max.x; i++) {
                IGridView.getCell(i, j).setState(stateChanges[j][i]);
            }
        }
    }
}
