package cluster.istruct.gol.game.automata;

import java.util.Random;

import cluster.istruct.gol.game.logic.ILogic;
import cluster.istruct.gol.game.unit.IUnit;
import cluster.istruct.gol.game.unit.IUnitPrefab;
import cluster.istruct.gol.game.grid.BoundlessIGridViewHandler;
import cluster.istruct.gol.game.grid.IGridView;
import cluster.istruct.gol.game.grid.IGridViewHandler;
import cluster.istruct.gol.game.converter.IGridConverter;
import cluster.istruct.gol.game.converter.ThreadedIGridConverter;

// AUTOMATA
abstract class lDiscreteITessellation<T extends IUnit> implements ITessellation<T> {

    protected final int gridSizeX;
    protected final int gridSizeY;
    final IGridViewHandler<T> IGridViewHandler;
    final IGridConverter<T> IGridConverter;
    ILogic<T> ILogic;

    public lDiscreteITessellation(int gridSizeX, int gridSizeY) {
        this.gridSizeX = gridSizeX;
        this.gridSizeY = gridSizeY;
        this.IGridViewHandler = getIGridViewHandler();
        this.IGridConverter = getIGridConverter();
        this.ILogic = createRule();
    }
    abstract protected IUnitPrefab<T> getFactory();

    @Override
    public void setILogic(ILogic<T> ILogic) {
        this.ILogic = ILogic;
    }

    @Override
    public final int getSizeX() {
        return gridSizeX;
    }

    @Override
    public final int getSizeY() {
        return gridSizeY;
    }
    protected abstract ILogic<T> createRule();

    protected BoundlessIGridViewHandler<T> getIGridViewHandler() {
        return new BoundlessIGridViewHandler<T>(gridSizeX, gridSizeY, getFactory());
    }

    protected IGridConverter<T> getIGridConverter() {
        return new ThreadedIGridConverter<T>();
    }

    @Override
    public void reset() {
        IGridView<T> IGridView = getCurrentState();
        T newBornCell = getFactory().create(0, 0);
        for (int j = 0; j < gridSizeY; j++) {
            for (int i = 0; i < gridSizeX; i++)
                IGridView.getCell(i, j).reset(newBornCell.getState());
        }
    }

    @Override
    public int getDefaultCellState() {
        return getFactory().create(0, 0).getState();
    }

    @Override
    public void randomFill(Filler filler) {
        IGridView<T> IGridView = getCurrentState();
        Random rnd = new Random();

        for (int j = 0; j < gridSizeY; j++) {
            for (int i = 0; i < gridSizeX; i++) {
                if (rnd.nextFloat() < filler.getRstio())
                    IGridView.getCell(i, j).setState(filler.getState());

            }
        }
    }

    @Override
    public void iterate() {
        IGridConverter.transform(IGridViewHandler.getCurrent(), ILogic);
    }

    @Override
    public final IGridView<T> getCurrentState() {
        return IGridViewHandler.getCurrent();
    }
}
