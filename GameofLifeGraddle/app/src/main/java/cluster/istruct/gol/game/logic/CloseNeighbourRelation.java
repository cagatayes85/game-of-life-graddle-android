package cluster.istruct.gol.game.logic;

import java.util.Set;

import cluster.istruct.gol.game.unit.IUnit;
import cluster.istruct.gol.game.unit.SimpleIUnit;
import cluster.istruct.gol.game.grid.IGridView;

public class CloseNeighbourRelation implements ILogic<SimpleIUnit> {
    private final Set<Integer> survivalNbCounts;
    private final Set<Integer> creationNbCounts;

    public CloseNeighbourRelation(Set<Integer> survivalNbCounts, Set<Integer> creationNbCounts) {
        this.survivalNbCounts = survivalNbCounts;
        this.creationNbCounts = creationNbCounts;
    }

    @Override
    public int apply(IGridView<SimpleIUnit> IGridView, int x, int y) {
        SimpleIUnit current = IGridView.getCell(x, y);
        int n = current.getNeighborCount();

        if (IGridView.getCell(x, y).isAlive()) {
            return staysAlive(n);

        } else {
            return getsCreated(n);
        }

    }

    private int staysAlive(int n) {
        if (survivalNbCounts.contains(n)) {
            return IUnit.LIVING;

        } else {
            return IUnit.DEAD;
        }
    }

    private int getsCreated(int n) {
        if (creationNbCounts.contains(n)) {
            return IUnit.LIVING;

        } else {
            return IUnit.DEAD;
        }
    }

    public Set<Integer> getSurvivalNbCounts() {
        return survivalNbCounts;
    }

    public Set<Integer> getCreationNbCounts() {
        return creationNbCounts;
    }
}
