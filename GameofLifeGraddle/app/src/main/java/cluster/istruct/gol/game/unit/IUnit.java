package cluster.istruct.gol.game.unit;

public interface IUnit {

    int LIVING = 1;
    int DEAD = 0;

    void reset(int state);
    long getId();
    int getY();
    int getX();
    void setState(int state);
    int getState();
    boolean isAlive();
    boolean isDead();
    void onNeighborStateChange(int newState);
    void setIUnitStateTrigger(IUnitStateTrigger IUnitStateTrigger);

}
