package cluster.istruct.gol.game.automata;

import cluster.istruct.gol.game.logic.ILogic;
import cluster.istruct.gol.game.unit.IUnit;
import cluster.istruct.gol.game.grid.IGridView;

public interface ITessellation<T extends IUnit> {

    void reset();

    void randomFill(Filler filler);

    void iterate();

    IGridView<T> getCurrentState();

    void setILogic(ILogic<T> ILogic);

    int getSizeX();

    int getSizeY();

    int getDefaultCellState();
}
