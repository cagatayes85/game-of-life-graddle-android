package cluster.istruct.gol.app.activity.main;

import android.app.Activity;
import android.widget.ImageButton;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Fullscreen;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.ViewById;

import cluster.istruct.gol.R;
import cluster.istruct.gol.game.view.TessellationView;

@EActivity(R.layout.activity_main)
@Fullscreen
public class MainActivity extends Activity {
    @InstanceState
    boolean paused;
    @Bean
    MainActHelper helper;

    @ViewById
    TessellationView teselView;

    @ViewById
    ImageButton reset, restart, pause, resume;

    @AfterViews
    void afterViews() {
        helper.startGame();
    }

    @Click
    void reset() {
        helper.onResetGame();
    }

    @Click
    void restart() {
        helper.onRestartGame();
    }


    @Click
    void pause() {
        helper.onPause();
    }

    @Click
    void resume() {
        helper.onResume();
    }

}
