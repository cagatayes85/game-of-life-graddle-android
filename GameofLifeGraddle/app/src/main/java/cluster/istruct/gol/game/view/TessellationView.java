package cluster.istruct.gol.game.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import cluster.istruct.gol.game.unit.IUnit;
import cluster.istruct.gol.game.automata.ITessellation;
import cluster.istruct.gol.game.controller.ParameterInitializer;
import cluster.istruct.gol.game.grid.IGridView;
import hugo.weaving.DebugLog;

//automatonView

public class TessellationView extends SurfaceView implements SurfaceHolder.Callback {
    private ITessellation automaton;
    private ParameterInitializer params;
    private TessellationThread thread;

    public TessellationView(Context context) {
        super(context);
    }

    public TessellationView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TessellationView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @DebugLog
    public void init(ITessellation automaton, ParameterInitializer params) {
        this.automaton = automaton;
        this.params = params;

        SurfaceHolder surfaceHolder = getHolder();
        surfaceHolder.addCallback(this);
    }

    @Override
    @DebugLog
    public void surfaceCreated(SurfaceHolder holder) {
        if (thread == null || thread.getState() == Thread.State.TERMINATED) {
            thread = new TessellationThread(automaton, holder, params);
            thread.start();
        }
        
        thread.setRunning(true);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        //
    }

    @Override
    @DebugLog
    public void surfaceDestroyed(SurfaceHolder holder) {
        thread.setRunning(false);
        waitForThreadToDie();
    }

    @DebugLog
    private void waitForThreadToDie() {
        while (true) {
            try {
                thread.join();
                break;
            } catch (InterruptedException ex) {
            }
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent evt) {
        paintBlockSelected(evt);

        return true;
    }

    protected void paintBlockSelected(MotionEvent event) {
        int x = Math.round(event.getX() / params.getCellSize());
        int y = Math.round(event.getY() / params.getCellSize());

        IGridView IGridView = automaton.getCurrentState();
        IGridView.getCell(x, y).setState(IUnit.LIVING);
        IGridView.getCell(x + 1, y).setState(IUnit.LIVING);
        IGridView.getCell(x, y + 1).setState(IUnit.LIVING);
        IGridView.getCell(x + 1, y + 1).setState(IUnit.LIVING);
    }
}
