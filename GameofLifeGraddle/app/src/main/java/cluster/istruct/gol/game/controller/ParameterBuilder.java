package cluster.istruct.gol.game.controller;

import android.graphics.Point;

import cluster.istruct.gol.game.automata.Filler;
import cluster.istruct.gol.game.painter.IPainter;

public class ParameterBuilder {
    private Point displaySize;
    private int cellSizeInPixels;
    private Filler filler;
    private IPainter iPainter;
    private int fps;

    public static ParameterBuilder create() {
        return new ParameterBuilder();
    }
    public ParameterInitializer create( Point displaySize , int cellSizeInPixels , Filler filler,IPainter iPainter, int fps ) {
        this.displaySize = displaySize;
        this.cellSizeInPixels = cellSizeInPixels;
        this.filler = filler;
        this.iPainter = iPainter;
        this.fps = fps;
        return new ParameterInitializer(displaySize, cellSizeInPixels, filler, iPainter, fps);
    }


    public ParameterBuilder setDisplaySize(Point displaySize) {
        this.displaySize = displaySize;
        return this;
    }

    public ParameterBuilder setCellSizeInPxFormat(int cellSizeInPixels) {
        this.cellSizeInPixels = cellSizeInPixels;
        return this;
    }

    public ParameterBuilder setFiller(Filler filler) {
        this.filler = filler;
        return this;
    }

    public ParameterBuilder setiPainter(IPainter iPainter) {
        this.iPainter = iPainter;
        return this;
    }

    public ParameterBuilder setFps(int fps) {
        this.fps = fps;
        return this;
    }

    public ParameterInitializer build() {
        return new ParameterInitializer(displaySize, cellSizeInPixels, filler, iPainter, fps);
    }
}