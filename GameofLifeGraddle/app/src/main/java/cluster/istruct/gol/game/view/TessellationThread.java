package cluster.istruct.gol.game.view;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.SurfaceHolder;

import com.squareup.otto.Subscribe;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

import cluster.istruct.gol.game.activity.EventRepository;
import cluster.istruct.gol.game.automata.ITessellation;
import cluster.istruct.gol.game.controller.ParameterInitializer;
import cluster.istruct.gol.game.painter.IPainter;
import cluster.istruct.gol.util.EventBus;
import hugo.weaving.DebugLog;

public class TessellationThread extends Thread {
    private final ITessellation automaton;
    private final SurfaceHolder surfaceHolder;
    private final int cellSizeInPixels;
    private final int timeForAFrame;
    private final IPainter iPainter;
    private final ParameterInitializer params;
    private boolean isRunning;
    private boolean shouldReset;
    private boolean shouldRestart;
    private boolean paused;
    private long cycleTime;
    private Queue<EventRepository.CellStateChange> cellStateChanges;
    private Bitmap buffCanvasBitmap;
    private Canvas buffCanvas;

    public TessellationThread(ITessellation automaton, SurfaceHolder surfaceHolder, ParameterInitializer params) {
        cellStateChanges = new LinkedBlockingQueue<>();
        this.automaton = automaton;
        this.params = params;
        this.surfaceHolder = surfaceHolder;
        this.cellSizeInPixels = params.getCellSize();
        timeForAFrame = 1000 / params.getFps();
        iPainter = params.getiPainter();

        buffCanvasBitmap = Bitmap.createBitmap(automaton.getSizeX() * cellSizeInPixels, automaton.getSizeY() * cellSizeInPixels, Bitmap.Config.ARGB_8888);
        buffCanvas = new Canvas();
        buffCanvas.setBitmap(buffCanvasBitmap);
    }

    @Subscribe
    synchronized public void onEvent(EventRepository.CellStateChange cellStateChange) {
        cellStateChanges.add(cellStateChange);
    }

    @Subscribe
    synchronized public void onEvent(EventRepository.Pause event) {
        paused = true;
    }

    @Subscribe
    synchronized public void onEvent(EventRepository.Resume event) {
        paused = false;
    }

    @Subscribe
    synchronized public void onEvent(EventRepository.Reset event) {
        shouldReset = true;
    }

    @Subscribe
    synchronized public void onEvent(EventRepository.Restart event) {
        shouldRestart = true;
    }

    public void setRunning(boolean v) {
        this.isRunning = v;
    }

    @Override
    public void run() {
        EventBus.getInstance().register(this);

        while (isRunning) {
            canvasCycle();
        }

        EventBus.getInstance().unregister(this);
    }

    protected void canvasCycle() {
        Canvas canvas = null;

        try {
            canvas = surfaceHolder.lockCanvas();
            gameCycle(canvas);

        } catch (InterruptedException e) {
            e.printStackTrace();

        } finally {
            unlockCanvasAndPost(canvas);
        }
    }

    protected void gameCycle(Canvas canvas) throws InterruptedException {
        if (canvas != null) {
            measuredCycleCore(canvas);
            sleepToKeepFps();
        }
    }

    protected void measuredCycleCore(Canvas canvas) {
        synchronized (surfaceHolder) {
            long t0 = System.currentTimeMillis();
            cycleCore(canvas);
            long t1 = System.currentTimeMillis();
            cycleTime = t1 - t0;
        }
    }

    @DebugLog
    private void cycleCore(Canvas canvas) {
        handleFlags();

        if (!paused) {
            stepAutomaton();
        }

        draw(canvas);
    }

    private void handleFlags() {
        handleReset();
        handleRestart();
        resetFlags();
    }

    private void handleReset() {
        if (shouldReset) {
            automaton.reset();
            clearCanvas(automaton.getDefaultCellState());
        }
    }

    private void clearCanvas(int state) {
        final int sizeX = automaton.getSizeX();
        final int sizeY = automaton.getSizeY();
        Rect rect = new Rect(0, 0, (sizeX + 1) * cellSizeInPixels, (sizeY + 1) * cellSizeInPixels);
        Paint paint = iPainter.getPaint(state);
        buffCanvas.drawRect(rect, paint);
    }

    private void handleRestart() {
        if (shouldRestart) {
            automaton.reset();
            automaton.randomFill(params.getFiller());
        }
    }

    private void resetFlags() {
        shouldReset = false;
        shouldRestart = false;
    }

    @DebugLog
    private void stepAutomaton() {
        automaton.iterate();
    }

    @DebugLog
    private void draw(Canvas canvas) {
        for (EventRepository.CellStateChange change : cellStateChanges) {
            paintCell(buffCanvas, change.x, change.y, change.stateSnapshot);
        }

        cellStateChanges.clear();
        canvas.drawBitmap(buffCanvasBitmap, 0, 0, null);
    }

    private void paintCell(Canvas canvas, int i, int j, int cellState) {
        Paint paint = iPainter.getPaint(cellState);
        Rect rect = new Rect(
                i*cellSizeInPixels,
                j*cellSizeInPixels,
                (i+1)*cellSizeInPixels,
                (j+1)*cellSizeInPixels
        );

        if (canvas != null) {
            canvas.drawRect(rect, paint);
        }
    }

    @DebugLog
    private void sleepToKeepFps() throws InterruptedException {
        long sleepTime = timeForAFrame - cycleTime;

        if (sleepTime > 0) {
            sleep(sleepTime);
        }
    }

    private void unlockCanvasAndPost(Canvas canvas) {
        if (canvas != null) {
            surfaceHolder.unlockCanvasAndPost(canvas);
        }
    }
}
