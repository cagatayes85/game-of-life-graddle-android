package cluster.istruct.gol.game.logic;

import java.util.Arrays;
import java.util.HashSet;

public class ConwayILogic extends CloseNeighbourRelation {
    public ConwayILogic() {
        super(
                new HashSet<>(Arrays.asList(2, 3)),
                new HashSet<>(Arrays.asList(3))
        );
    }
}
