package cluster.istruct.gol.game.grid;

import java.util.HashSet;
import java.util.Set;

import cluster.istruct.gol.game.unit.IUnit;
import cluster.istruct.gol.game.unit.IUnitPrefab;
import cluster.istruct.gol.game.unit.IUnitStateTrigger;
import cluster.istruct.gol.game.activity.EventRepository;
import cluster.istruct.gol.util.EventBus;

public class BoundlessGridView<T extends IUnit> implements IGridView<T>, IUnitStateTrigger {
    protected final int sizeX;
    protected final int sizeY;
    protected final T[][] cells;
    protected final Set<Long> cellIds;

    public BoundlessGridView(int sizeX, int sizeY, IUnitPrefab<T> IUnitPrefab) {
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        cells = (T[][]) new IUnit[sizeY][sizeX];
        cellIds = new HashSet<>(sizeY * sizeX);
        createCells(sizeX, sizeY, IUnitPrefab);
    }

    public BoundlessGridView(IGridView<T> other, IUnitPrefab<T> IUnitPrefab) {
        this(other.getSizeX(), other.getSizeY(), IUnitPrefab);
        copyCells(other);
    }

    protected void createCells(int sizeX, int sizeY, IUnitPrefab<T> IUnitPrefab) {
        for (int j = 0; j < sizeY; j++) {
            for (int i = 0; i < sizeX; i++) {
                T cell = IUnitPrefab.create(i, j);
                putCell(cell);
            }
        }
    }

    protected void copyCells(IGridView<T> other) {
        for (int j = 0; j < sizeY; j++) {
            for (int i = 0; i < sizeX; i++) {
                putCell(other.getCell(i, j));
            }
        }
    }

    @Override
    public void putCell(T cell) {
        int y = normalizeY(cell.getY());
        int x = normalizeX(cell.getX());
        maintainIds(cell, x, y);
        cell.setIUnitStateTrigger(this);

        if (cells[y][x] != null) {
            cells[y][x].setIUnitStateTrigger(null);
        }

        cells[y][x] = cell;
    }

    private void maintainIds(T cell, int x, int y) {
        if (cells[y][x] != null) {
            cellIds.remove(cells[y][x].getId());
        }

        cellIds.add(cell.getId());
    }

    @Override
    public void onUnitStateChanged(EventRepository.CellStateChange cellStateChange) {
        EventBus.getInstance().post(cellStateChange);

        if (cellIds.contains(cellStateChange.IUnit.getId())) {
            notifyNeighbors(cellStateChange.IUnit);
        }
    }

    private void notifyNeighbors(IUnit IUnit) {
        for (int j = -1; j <= 1; j++) {
            for (int i = -1; i <= 1; i++) {
                if (j == 0 && i == 0) {
                    continue;
                }

                IUnit neighbor = getCell(IUnit.getX() + i, IUnit.getY() + j);
                neighbor.onNeighborStateChange(IUnit.getState());
            }
        }
    }

    @Override
    public int getSizeX() {
        return sizeX;
    }

    @Override
    public int getSizeY() {
        return sizeY;
    }

    @Override
    public T getCell(int x, int y) {
        return (T) cells[normalizeY(y)][normalizeX(x)];
    }

    int normalizeY(int y) {
        while (y < 0) {
            y += sizeY;
        }

        while (y >= sizeY) {
            y -= sizeY;
        }

        return y;
    }

    int normalizeX(int x) {
        while (x < 0) {
            x += sizeX;
        }

        while (x >= sizeX) {
            x -= sizeX;
        }

        return x;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BoundlessGridView that = (BoundlessGridView) o;

        if (sizeX != that.sizeX) return false;
        if (sizeY != that.sizeY) return false;

        for (int j = 0; j < getSizeY(); j++) {
            for (int i = 0; i < getSizeX(); i++) {
                IUnit IUnit = getCell(i, j);
                IUnit otherIUnit = that.getCell(i, j);

                if (!otherIUnit.equals(IUnit)) {
                    debugOnCellsNotEqual(IUnit, otherIUnit);
                    return false;
                }
            }
        }

        return true;
    }

    protected void debugOnCellsNotEqual(IUnit IUnit, IUnit otherIUnit) {
    }

    @Override
    public int hashCode() {
        int result = sizeX;
        result = 31 * result + sizeY;

        return result;
    }
}
