package cluster.istruct.gol.game.converter;

import cluster.istruct.gol.game.logic.ILogic;
import cluster.istruct.gol.game.unit.IUnit;
import cluster.istruct.gol.game.grid.IGridView;
import hugo.weaving.DebugLog;

public class GridConverter<T extends IUnit> implements IGridConverter<T> {
    private int[][] stateChanges;

    @Override
    public final void transform(IGridView<T> IGridView, ILogic<T> ILogic) {
        stateChanges = new int[IGridView.getSizeY()][IGridView.getSizeX()];
        computeNewGrid(IGridView, ILogic);
        applyNewGrid(IGridView);
    }

    @DebugLog
    protected void computeNewGrid(IGridView<T> IGridView, ILogic<T> ILogic) {
        for (int j = 0; j < IGridView.getSizeY(); j++) {
            for (int i = 0; i < IGridView.getSizeX(); i++) {
                stateChanges[j][i] = ILogic.apply(IGridView, i, j);
            }
        }
    }

    @DebugLog
    protected void applyNewGrid(IGridView<T> IGridView) {
        for (int j = 0; j < IGridView.getSizeY(); j++) {
            for (int i = 0; i < IGridView.getSizeX(); i++) {
                IGridView.getCell(i, j).setState(stateChanges[j][i]);
            }
        }
    }
}
