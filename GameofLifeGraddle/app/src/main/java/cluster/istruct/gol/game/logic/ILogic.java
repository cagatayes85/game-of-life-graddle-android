package cluster.istruct.gol.game.logic;

import cluster.istruct.gol.game.unit.IUnit;
import cluster.istruct.gol.game.grid.IGridView;

public interface ILogic<T extends IUnit> {
    int apply(IGridView<T> IGridView, int x, int y);
}
