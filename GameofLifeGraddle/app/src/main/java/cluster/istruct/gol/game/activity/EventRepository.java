package cluster.istruct.gol.game.activity;

import cluster.istruct.gol.game.unit.IUnit;


public  class EventRepository {
    private static EventRepository _basicInstance = new EventRepository();

    public static EventRepository getBasics(){
    return _basicInstance;
    }

    public class Pause {
        //TO DO
    }
    public class Reset {
        //TO DO
    }
    public class Restart {
        //TO DO
    }
    public class Resume {
        //TO DO
    }

    public class CellStateChange {
        public final int x;
        public final int y;
        public final int stateSnapshot;
        public final IUnit IUnit;

        public CellStateChange(IUnit IUnit) {
            this.IUnit = IUnit;
            x = IUnit.getX();
            y = IUnit.getY();
            stateSnapshot = IUnit.getState();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            CellStateChange that = (CellStateChange) o;

            if (stateSnapshot != that.stateSnapshot) return false;
            if (x != that.x) return false;
            if (y != that.y) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = x;
            result = 31 * result + y;
            result = 31 * result + stateSnapshot;
            return result;
        }

    }

}
