package cluster.istruct.gol.game.grid;

import cluster.istruct.gol.game.unit.IUnit;

public interface IGridViewHandler<T extends IUnit> {
    IGridView<T> getCurrent();
    IGridView<T> createNew();
    void setCurrent(IGridView<T> IGridView);
}
